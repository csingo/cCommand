package cCommand

import (
	"sync"

	"github.com/robfig/cron/v3"
)

type CommandMode int

const (
	CommandMode_Single CommandMode = iota // 异步串行
	CommandMode_Multi                     // 异步并行
	CommandMode_Sync                      // 同步单次执行
	CommandMode_Line                      // 命令行执行
)

const (
	CommandConfigName = "CommandConf"
)

type CommandConf struct {
	Enable      bool                   `json:"enable"`
	CommandLine bool                   `json:"command_line"`
	Commands    []*CommandConf_Command `json:"commands"`
}

type CommandConf_Command struct {
	// 初始化项
	id cron.EntryID // 定时执行ID
	// index int          // 配置下标
	total int        // 已执行次数
	lock  sync.Mutex // 串行控制

	// 配置项
	Mode    CommandMode `json:"mode"`    // 执行模式
	Instant bool        `json:"instant"` // 是否立即执行, sync 模式忽略此参数, 异步模式先执行一次, 再启动定时任务
	Wait    int         `json:"wait"`    // 每次执行等待时长
	Try     int         `json:"try"`     // 执行次数
	Cron    string      `json:"cron"`    // 定时器
	App     string      `json:"app"`
	Command string      `json:"command"`
	Method  string      `json:"method"`
	Options []string    `json:"options"` // 调用参数, 按顺序传入
}

func (i *CommandConf_Command) SetID(id cron.EntryID) {
	// data, err := json.Marshal(i)
	// if err != nil {
	// 	return "", err
	// }
	//
	// encode := base64.StdEncoding.EncodeToString(data)
	// result := fmt.Sprintf(
	// 	"%d:%s:%s:%s:%s",
	// 	i.index,
	// 	i.App,
	// 	i.Command,
	// 	i.Method,
	// 	encode,
	// )
	//
	// return result, nil
	i.id = id
}

// func (i *CommandConf_Command) Index(index int) {
// 	i.index = index
// }

func (i *CommandConf_Command) Incr() {
	i.total++
}

func (i *CommandConf_Command) Runnable() bool {
	return (i.total < i.Try && i.Try > 0) || i.Try <= 0
}

func (i *CommandConf_Command) TryLock() bool {
	return i.lock.TryLock()
}

func (i *CommandConf_Command) Unlock() {
	i.lock.Unlock()
}

func (i *CommandConf) ConfigName() string {
	return CommandConfigName
}

var command_config *CommandConf
