package cCommand

type CommandInterface interface {
	CommandName() (app, name string)
}
