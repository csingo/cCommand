package cCommand

import (
	"github.com/robfig/cron/v3"

	"gitee.com/csingo/cComponents"
)

type CommandComponent struct{}

func (i *CommandComponent) Inject(instance any) bool {
	if container.Is(instance) {
		return container.Save(instance)
	}

	return false
}

func (i *CommandComponent) InjectConf(config cComponents.ConfigInterface) bool {
	if config.ConfigName() == CommandConfigName {
		command_config = config.(*CommandConf)
		container.UpdateCommands()
		return true
	}

	return false
}

func (i *CommandComponent) Load() {
	exec()

	if !command_config.Enable {
		return
	}

	// 创建定时器
	container.cron = cron.New(
		cron.WithSeconds(),
		cron.WithChain(
			cron.Recover(cron.DefaultLogger),
		),
	)

	// 任务分发执行
	dispatch()

	// 清除已停止的任务
	go clears()
}

func (i *CommandComponent) Listen() []*cComponents.ConfigListener {
	return []*cComponents.ConfigListener{}
}

var Component = &CommandComponent{}
