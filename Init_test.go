package cCommand

import (
	"log"
	"testing"
	"time"

	"gitee.com/csingo/cLog"
)

type TestCommand struct{}

func (t *TestCommand) CommandName() (app, name string) {
	return "test", "test"
}

func (t *TestCommand) Hello(name string) error {
	log.Println("hello world", name)
	return nil
}

func TestInit(t *testing.T) {
	cLog.Component.Load()
	Component.Inject(&TestCommand{})
	Component.InjectConf(&CommandConf{
		Enable: true,
		Commands: []*CommandConf_Command{
			{
				Mode:    CommandMode_Single,
				Instant: true,
				Wait:    0,
				Try:     2,
				Cron:    "* * * * * *",
				App:     "test",
				Command: "test",
				Method:  "Hello",
				Options: []string{
					"cxy01",
				},
			},
			{
				Mode:    CommandMode_Single,
				Instant: false,
				Wait:    0,
				Try:     0,
				Cron:    "* * * * * *",
				App:     "test",
				Command: "test",
				Method:  "Hello",
				Options: []string{
					"cxy02",
				},
			},
		},
	})
	Component.Load()

	time.Sleep(1 * time.Hour)
}
